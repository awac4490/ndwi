package tutorial

import java.awt.image.BufferedImage
import java.io._
import javax.imageio.ImageIO

object blob {
  var ruta: String = CreateNDWIPng.ndwiPath
  var archivo = CreateNDWIPng.nameArchivo

  var alto = 0
  var ancho = 0
  var equivalenciasR = new Array[Int](5000000)
  var etiquetasSegmentar = new Array[Int](5000000)
  val umbralDiferente = 10

  //  Initializing the label matrix
  var matrixR: Array[Array[Int]] = Array.ofDim[Int](ancho, alto)

  // Matriz de pixeles para archivo
  var pixelesArchivo: Array[Int] = Array.ofDim[Int](ancho)

  val areaMinimaBarco = 0
  var areaMaximaBarco = 10000

  def main(args: Array[String]): Unit = {
    if (args.length != 0){
      archivo = args(0)
      ruta = "D:\\test\\result\\" + archivo + "\\"
    }

    println(ruta + archivo + "_otsuM.png")
    val image = ImageIO.read(new File(ruta + archivo + "_otsuM.png"))
    println(ruta + archivo + "_rgb.png")
    val imageOriginal = ImageIO.read(new File(ruta + archivo + "_rgb.png"))
    alto = image.getHeight
    ancho = image.getWidth

    areaMaximaBarco = (alto * ancho) -1
    matrixR = Array.ofDim[Int](ancho, alto)
    equivalenciasR = new Array[Int](5000000)
    etiquetasSegmentar = new Array[Int](5000000)
    pixelesArchivo = Array.ofDim[Int](ancho)

    // Execution of the method: Generation of the matrix
    val etiquetas = secuencial(image)
    // println(etiquetas + " found tags")

    var candidatesShip = 0
    for (etiqueta <- 1 until etiquetas) {
      if (etiqueta % 100 == 0)
        print(etiqueta + "...")
      val areaEtiqueta = areaOcupada(etiqueta)
      if (areaEtiqueta != 0 && areaEtiqueta >= areaMinimaBarco && areaEtiqueta <= areaMaximaBarco) {
        etiquetasSegmentar(candidatesShip) = etiqueta
        candidatesShip = candidatesShip + 1
      }
    }
    // println(etiquetas)

    pixelesArchivo = Array.ofDim(candidatesShip * 4)

    for (x <- 0 until candidatesShip) {
      // print("Segmenting: " + etiquetasSegmentar(x) + " -> ")
      segmentarPosibleBarco(x, etiquetasSegmentar(x), imageOriginal)
    }

    // println("Tags found: " + etiquetas)
    // println("Total detected objects: " + contarObjetos(etiquetas))
    println("Candidates to ship: " + candidatesShip)

    printToFile(new File(ruta + "test.txt")) {
      p => pixelesArchivo.foreach(p.println)
    }

  }

  def segmentarPosibleBarco(numeroObject: Int, indice: Int, image: BufferedImage): Unit = {
    val borde = 5
    var minX = getMinX(image, indice)
    minX = if (minX >= borde) minX - borde else 0

    var minY = getMinY(image, indice)
    minY = if (minY >= borde) minY - borde else 0

    var maxX = getMaxX(image, indice)
    maxX = if (maxX + borde > image.getWidth) image.getWidth - 1 else maxX + borde

    var maxY = getMaxY(image, indice)
    maxY = if (maxY + borde > image.getHeight) image.getHeight - 1 else maxY + borde

    // println(minX + ", " + minY + ", " + maxX + ", " + maxY)
    // add pixels archivo
    pixelesArchivo(numeroObject * 4) = minX
    pixelesArchivo(numeroObject * 4 + 1) = minY
    pixelesArchivo(numeroObject * 4 + 2) = maxX
    pixelesArchivo(numeroObject * 4 + 3) = maxY

    val subImagen = image.getSubimage(minX, minY, maxX - minX, maxY - minY)
    val name = minX + "_" + minY + "_" + maxX + "_" + maxY
    ImageIO.write(subImagen, "png", new File(ruta + "blob_" + name + ".png"))
  }

  def getMinX(imagen: BufferedImage, indice: Int): Int = {
    var minX = imagen.getWidth - 1
    var found = false
    var x = 0
    while ( {
      !found && x < imagen.getWidth
    }) {
      var y = 0
      while ( {
        !found && y < imagen.getHeight
      }) {
        if (matrixR(x)(y) == indice && x < minX) {
          minX = x
          found = true
        }

        {
          y += 1
          y - 1
        }
      }

      {
        x += 1
        x - 1
      }
    }
    minX
  }

  def getMinY(imagen: BufferedImage, indice: Int): Int = {
    var minY = imagen.getHeight - 1
    var found = false
    var y = 0
    while ( {
      !found && y < imagen.getHeight
    }) {
      var x = 0
      while ( {
        !found && x < imagen.getWidth
      }) {
        if (matrixR(x)(y) == indice && y < minY) {
          minY = y
          found = true
        }

        {
          x += 1
          x - 1
        }
      }

      {
        y += 1
        y - 1
      }
    }
    minY
  }

  def getMaxX(imagen: BufferedImage, indice: Int): Int = {
    var maxX = 0
    var found = false
    var x = imagen.getWidth - 1
    while ( {
      !found && x >= 0
    }) {
      var y = 0
      while ( {
        !found && y < imagen.getHeight
      }) {
        if (matrixR(x)(y) == indice && x > maxX) {
          maxX = x
          found = true
        }

        {
          y += 1
          y - 1
        }
      }

      {
        x -= 1
        x + 1
      }
    }
    maxX
  }

  def getMaxY(imagen: BufferedImage, indice: Int): Int = {
    var maxY = 0
    val found = false
    var y = imagen.getHeight - 1
    while ( {
      !found && y >= 0
    }) {
      var x = 0
      while ( {
        !found && x < imagen.getWidth
      }) {
        if (matrixR(x)(y) == indice && y > maxY) maxY = y

        {
          x += 1
          x - 1
        }
      }

      {
        y -= 1
        y + 1
      }
    }
    maxY
  }

  def areaOcupada(i: Int): Int = {
    var resp = 0
    for (x <- 0 until alto) {
      for (y <- 0 until ancho) {
        if (matrixR(y)(x) == i)
          resp = resp + 1
        if (resp > areaMaximaBarco)
          return areaMaximaBarco + 1
      }
    }
    resp
  }

  def printToFile(f: java.io.File)(op: java.io.PrintWriter => Unit) {
    val p = new java.io.PrintWriter(f)
    try op(p) finally p.close()
  }

  def verMatrix() {
    for (x <- 0 until alto) {
      for (y <- 0 until ancho) {
        print(matrixR(y)(x) + "\t")
      }
      println()
    }
  }

  def secuencial(image: BufferedImage): Int = {
    var numeroObjeto = 1

    for (x <- 0 until ancho) {
      for (y <- 0 until alto) {
        if (matrixR(x)(y) == 0 && image.getRGB(x, y) != 0) {
          val actual = image.getRGB(x, y)

          val xSup = x - 1
          val ySup = y

          val xIzq = x
          val yIzq = y - 1

          var superior = 0
          var izquierdo = 0
          if (xSup >= 0 && ySup >= 0) {
            superior = image.getRGB(xSup, ySup)
          }

          if (xIzq >= 0 && yIzq >= 0) {
            izquierdo = image.getRGB(xIzq, yIzq)
          }

          val auxSupIzq = diferenciaColores(superior, izquierdo) <= umbralDiferente
          if (superior != 0 && izquierdo != 0 && auxSupIzq) { // Vecinos iguales
            if (matrixR(xSup)(ySup) == matrixR(xIzq)(yIzq)) { // Mismas etiquetas
              val auxSuperior = diferenciaColores(actual, superior) <= umbralDiferente
              if (auxSuperior) {
                matrixR(x)(y) = matrixR(xSup)(ySup)
              } else {
                matrixR(x)(y) = numeroObjeto
                numeroObjeto += 1
              }
            } else { // etiquetas diferentes
              equivalenciasR(Math.max(matrixR(xSup)(ySup), matrixR(xIzq)(yIzq))) = Math.min(matrixR(xSup)(ySup), matrixR(xIzq)(yIzq))
              val auxSuperior = diferenciaColores(actual, superior) <= umbralDiferente
              if (auxSuperior) {
                matrixR(x)(y) = Math.min(matrixR(xSup)(ySup), matrixR(xIzq)(yIzq))
              } else {
                matrixR(x)(y) = numeroObjeto
                numeroObjeto += 1
              }
            }
          } else { // Vecinos diferentes
            val auxSuperior = diferenciaColores(actual, superior) <= umbralDiferente
            val auxIzquierdo = diferenciaColores(actual, izquierdo) <= umbralDiferente
            if (superior != 0 && auxSuperior) { // Actual igual al de arriba
              matrixR(x)(y) = matrixR(xSup)(ySup)
            } else if (izquierdo != 0 && auxIzquierdo) { // Actual igual al izquierdo
              matrixR(x)(y) = matrixR(xIzq)(yIzq)
            } else { // Actual diferente a ambos
              matrixR(x)(y) = numeroObjeto
              numeroObjeto += 1
            }
          }
        }
      }
    }
    completeSequential()
    numeroObjeto
  }

  def diferenciaColores(color: Int, colAux: Int): Float = {
    if (color == colAux)
      return 0
    val red1 = (color >> 16) & 0xff
    val green1 = (color >> 8) & 0xff
    val blue1 = color & 0xff

    val red2 = (colAux >> 16) & 0xff
    val green2 = (colAux >> 8) & 0xff
    val blue2 = colAux & 0xff

    val diffRed = Math.abs(red1 - red2)
    val diffGreen = Math.abs(green1 - green2)
    val diffBlue = Math.abs(blue1 - blue2)

    val pctDiffRed = diffRed.toFloat / 255
    val pctDiffGreen = diffGreen.toFloat / 255
    val pctDiffBlue = diffBlue.toFloat / 255

    val diferencia = (pctDiffRed + pctDiffGreen + pctDiffBlue) / 3 * 100
    diferencia

  }

  def completeSequential() {
    for (x <- 0 until ancho) {
      for (y <- 0 until alto) {
        matrixR(x)(y) = SearchEquivalence(matrixR(x)(y))
      }
    }
  }

  def SearchEquivalence(numero: Int): Int = {
    val resp = numero
    if (equivalenciasR(numero) == 0 || numero == 0)
      resp
    else SearchEquivalence(equivalenciasR(numero))
  }

  def contarObjetos(limite: Int): Int = {
    var resp = 0
    for (x <- 1 until limite) {
      if (equivalenciasR(x) == 0) {
        resp = resp + 1
      }
    }
    resp
  }

}
