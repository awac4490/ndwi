package tutorial

import geotrellis.raster._
import geotrellis.raster.io.geotiff._
import geotrellis.raster.render._
import com.typesafe.config.ConfigFactory

object CreateNDWIPng {

  val R_BAND = 2
  val G_BAND = 1
  val NIR_BAND = 3
  // val maskedPath = "/home/aaron/Documentos/Proyectos/test/img1.TIF"
  // val ndwiPath = "/home/aaron/Documentos/Proyectos/test/result/"
  val maskedPath = "E:\\test\\in\\"
  var nameArchivo = "rec.tif"
  var ndwiPath = "E:\\test\\out\\"
  val namePath = "ndwi"
  // val extend = new Extent(x0, y0, x1, y1)
  val divisiones = 1

  def main(args: Array[String]): Unit = {
    println("Generando NDWI ...")
    try {
      if (args.length != 0){
        nameArchivo = args(0)
        ndwiPath = "D:\\test\\result\\" + nameArchivo+"\\"
      }

      println("Origen: " + maskedPath + nameArchivo)
      println("Destino: " + ndwiPath)

      var crs = MultibandGeoTiff(maskedPath + nameArchivo)
      println("CRS: " + crs.crs)
      // println("Extend: " + crs.extent)
      val imgPrincipal = crs.tile

      val divAncho = imgPrincipal.cols / divisiones
      val divAlto = imgPrincipal.rows / divisiones
      for (i <- 0 until divisiones) {
        val yMin = i * divAncho
        val yMax = (i + 1) * divAncho
        for (j <- 0 until divisiones) {
          val xMin = j * divAlto
          val xMax = (j + 1) * divAlto

          // Cutting the image according to the requested parameters and
          val cutout = imgPrincipal.crop(yMin, xMin, yMax, xMax)

          println("Iniciando NDWI...")
          val ndwi = {
            // Converting the mosaic to write double values, since we perform an operation that produces floating point values.
            val test = cutout.convert(DoubleConstantNoDataCellType)

            // Use the combineDouble method to map over the infrared and red values ​​and perform the NDVI calculation.
            test.combineDouble(G_BAND, NIR_BAND) { (g: Double, ir: Double) => Calculations.ndwi(g, ir);
            }
          }

          // Obtain color map from the application.conf configuration file.
          val colorMap = ColorMap.fromStringDouble(ConfigFactory.load().getString("tutorial.ndwiColormap")).get

          // Process this NDWI using the color breaks as PNG and write the PNG on the disk.
          println(ndwiPath + nameArchivo + "_ndwi.png")
          ndwi.renderPng(colorMap).write(ndwiPath + nameArchivo + "_ndwi.png")

          // Generating RGB TIF trim
          val rgb = cutout.convert(DoubleCellType).combineDouble(0, 1, 2) { (r, g, b) =>
            if (0.0 == r && 0.0 == g && 0.0 == b) {
              Double.NaN
            } else {
              val b_temp = (b.toInt * 255) / 2095
              val g_temp = (g.toInt * 255) / 2095
              val r_temp = (r.toInt * 255) / 2095

              val temp = 0xFFFFFFFF & (255 | b_temp << 8 | g_temp << 16 | r_temp << 24)
              if (isData(r) && isData(g) && isData(b)) {
                temp
              } else {
                Double.NaN
              }
            }
          }
          // Rendering and saving PNG to disk
          rgb.renderPng().write(ndwiPath + nameArchivo + "_rgb.png")

        }
      }
      println("Ended process")
    } catch {
      case unknown => println(unknown)
    }
  }
}
