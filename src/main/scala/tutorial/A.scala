package tutorial

import java.io.File

object A {
  def main(args: Array[String]) {
    val rutaActual = "D:\\test\\nps"
    val directorioActual = new File(rutaActual)
    val archivos = directorioActual.listFiles

    println("Inicio...")

    for (archivo <- archivos) {
      if (archivo.getName.contains(".tif")) {
        val nombreArchivo = archivo.getName
        val carpeta = crearCarpeta(nombreArchivo)
        println("creada " + carpeta.getName)
        val arg = new Array[String](2)
        arg(0) = nombreArchivo
        CreateNDWIPng.main(arg)
        // Otsu.main(arg)
        // blob.main(arg)
      }
    }
  }

  def crearCarpeta(nameFile: String) = {
    val directorio = new File("D:\\test\\result\\" + nameFile)
    directorio.mkdir
    directorio
  }
}
