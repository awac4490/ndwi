package tutorial

import java.awt.Color
import java.awt.image.BufferedImage
import java.io.File
import java.util

import javax.imageio.ImageIO

import scala.util.control.Breaks

object Otsu {
  def main(args: Array[String]) {
    // var ruta = "D:\\test\\result\\"
    var rutaIn = "D:\\test\\result\\"
    var ruta = "D:\\test\\result\\"
    var name = ""
    if (args.length != 0){
      name = args(0)
      rutaIn += args(0) +"\\"
    } else {
      name = CreateNDWIPng.nameArchivo
    }
    // println("Initiate Otsu ...")

    // println(ruta + args(0))
    // val image = ImageIO.read(new File(ruta + name + "_ndwi.png"))
    println(ruta+name)
    val image = ImageIO.read(new File(ruta + name))
    val respMe = mediana(image, 5)
    // ImageIO.write(respMe, "png", new File(ruta + name + "_med.png"))
    ImageIO.write(respMe, "png", new File(rutaIn + "med.png"))
    var respOtsu = otsu(image)
    println("Umbral: " + respOtsu)
    val outO = binarizar(respMe, respOtsu)
    respOtsu = 110
    val outM = binarizar(respMe, respOtsu)
    // val coll = binarizarColor(image)
    // ImageIO.write(coll, "png", new File(ruta + name + "_otsuC.png" ))
    ImageIO.write(outO, "png", new File(rutaIn + name +  "_otsu.png" ))
    ImageIO.write(outM, "png", new File(rutaIn +name + "_otsuM.png" ))
    println("Otsu realized")
  }

  def mediana(image: BufferedImage, kernel: Int): BufferedImage ={
    val alto = image.getHeight
    val ancho = image.getWidth
    val out = new BufferedImage(ancho, alto, BufferedImage.TYPE_INT_ARGB)
    val matriz = Array.ofDim[Int](ancho, alto)

    for (i <- 0 until ancho) {
      for (j <- 0 until alto) {
        matriz(i)(j) = image.getRGB(i, j)
      }
    }

    val inicio = kernel / 2
    for (i <- 0 until ancho) {
      for (j <- 0 until alto) {
        if (image.getRGB(i, j) == 0) {
          out.setRGB(i, j, 0)
        } else {
          val nums = new Array[Int](kernel * kernel)
          var cont = 0
          for (x <- i - inicio until i + inicio) {
            for (y <- j - inicio until j + inicio) {
              if (x >= 0 && y >= 0 && x < ancho && y < alto){
                nums(cont) = matriz(x)(y)
              } else {
                nums(cont) = 0
              }
              cont = cont + 1
            }
          }
          util.Arrays.sort(nums)
          out.setRGB(i, j, nums(4))
        }
      }
    }

    /** for (i <- 1 until ancho -1) {
      for (j <- 1 until alto -1) {
        if (image.getRGB(i, j) == 0){
          out.setRGB(i, j, 0)
        } else {
          val nums = new Array[Int](9)
          nums(0) = matriz(i - 1)(j - 1)
          nums(1) = matriz(i - 1)(j)
          nums(2) = matriz(i - 1)(j + 1)
          nums(3) = matriz(i)(j)
          nums(4) = matriz(i)(j - 1)
          nums(5) = matriz(i)(j + 1)
          nums(6) = matriz(i + 1)(j)
          nums(7) = matriz(i + 1)(j - 1)
          nums(8) = matriz(i + 1)(j + 1)
          util.Arrays.sort(nums)
          out.setRGB(i, j, nums(4))
        }
      }
    }*/
    out
  }

  def otsu(img: BufferedImage): Int = {
    val nbHistLevels = 256
    val histData = new Array[Int](nbHistLevels)
    //calculate histogram
    for (i <- 0 until nbHistLevels) {
      histData(i) = 0
    }

    for (i <- 0 until img.getHeight()) {
      for (j <- 0 until img.getWidth()) {
        val pix = new Color(img.getRGB(j, i))
        var auss = 0.30270304 * pix.getRed
        auss = auss + 0.587162608 * pix.getGreen
        auss = auss + 0.110120159 * pix.getBlue
        val promedio = Math.round(auss).toInt
        histData(promedio) = histData(promedio) + 1
      }
    }

    val total = img.getWidth * img.getHeight
    var sum = 0
    var t = 0
    for (t <- 0 until nbHistLevels) {
      sum += t * histData(t)
    }

    var sumB = 0.toFloat
    var wB = 0.toFloat
    var wF = 0.toFloat
    var varMax = 0.toFloat
    var threshold = 0
    val loop = new Breaks
    val inner = new Breaks

    t = 0
    loop.breakable {
      for (t <- 0 until nbHistLevels) {
        wB = wB + histData(t) // Weight Background

        inner.breakable {
          if (wB == 0)
            inner.break

          wF = total - wB // Weight Foreground
          if (wF == 0)
            loop.break

          sumB = sumB + (t * histData(t))
          val mB = sumB / wB // Mean Background
          val mF = (sum - sumB) / wF // Mean Foreground

          // Calculate Between Class Variance
          val varBetween = wB * wF * (mB - mF) * (mB - mF)
          // println("wB: " + wB + ", wf: " + wF + ", mB: " + mB+ ", mF: " + mF)
          // Check if new maximum found
          if (varBetween > varMax) {
            varMax = varBetween
            threshold = t
          }
        }
      }
    }

    threshold
  }

  def binarizar(image: BufferedImage, umbral: Float): BufferedImage = {
    val alto = image.getHeight
    val ancho = image.getWidth
    val out = new BufferedImage(ancho, alto, BufferedImage.TYPE_INT_ARGB)
    for (i <- 0 until ancho) {
      for (j <- 0 until alto) {
        val pix = new Color(image.getRGB(i, j))
        var promedio = 0.30270304 * pix.getRed
        promedio = promedio + 0.587162608 * pix.getGreen
        promedio = promedio + 0.110120159 * pix.getBlue

        var color = new Color(255, 255, 255).getRGB; // Blanco

        if (promedio <= umbral) { // negro
          color = 0
        }
        out.setRGB(i, j, color)
      }
    }
    out
  }

  def binarizarColor(image: BufferedImage): BufferedImage = {
    val alto = image.getHeight
    val ancho = image.getWidth
    println(alto +" "+ ancho)
    val out = new BufferedImage(ancho, alto, BufferedImage.TYPE_INT_ARGB)
    for (i <- 0 until ancho) {
      for (j <- 0 until alto) {
        var color = new Color(255, 255, 255).getRGB // Blanco
        // print(new Color(image.getRGB(i, j))+" ")
        if (image.getRGB(i, j) == 0){
          color = 0
        }
        out.setRGB(i, j, color)
      }
      // println()
    }
    out
  }
}
