# Modificación

### Create a PNG of an NDWI of our image.

The code in `src/main/scala/tutorial/CreateNDWIPng.scala` will do this.

```console
> ./sbt run
```

Select the `tutorial.CreateNDWIPng` to run.

This will produce `ndwi.png`

### Create OTSU output of a PNG image

The code is in the `src/main/scala/tutorial/Otsu.scala` file.

```console
> ./sbt run
```

Select the `tutorial.Otsu` to run.
This will produce `otsu.png`

### Create BLOB output from a PNG image

The code is located in the `src/main/scala/tutorial/blob.scala` file.

```console
> ./sbt run
```

Select the `tutorial.blob` to run.
